const { User } = require('../models');
const { response } = require('../utils/response');
const bcrypt = require('bcrypt');
const jwtService = require('./jwtService');

const authService = {
    async register(userData){
        try {
            const { username, password } = userData;

            const exist = await User.count({where: {username}});
            if(!!exist) return response('FAIL', {message: "Username does exist"}, {});
            console.log(!!exist)

            const salt = bcrypt.genSaltSync(10);
            const hash = bcrypt.hashSync(password, salt);

            const user = await User.create({
                username,
                password: hash,
                salt
            });

            return response('OK', {}, {id: user.id});
        } catch (err) {
            return response('ERROR', {message: err.message}, {});
        }
        
    },
    async login(userData){
        try {
            const { username, password } = userData;
            const exist = await User.findOne({where: {username}});
            if(!exist) return response('FAIL', {message: "User not found!"}, {});

            const user = await User.findOne({where: {username}});

            if(user.password === bcrypt.hashSync(password, user.salt)){
                const {status, error, data} = jwtService.genToken(user.id);
                console.log(data)
                return response('OK', {}, {token: data.token})
            }else{
                return response('FAIL', {message: "Incorrect password!"}, {});
            }

        } catch (err) {
            return response('ERROR', {message: err.message});
        }
    }
}

module.exports = authService;