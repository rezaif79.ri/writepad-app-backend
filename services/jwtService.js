const jwt = require('jsonwebtoken');
const { response } = require('../utils/response');

const jwtService = {
    genToken(userId){
        try {
            const token = jwt.sign({id: userId}, process.env.SECRET_KEY, { algorithm: "HS512", expiresIn: '1h' });
            // console.log(token)
            // console.log('test')
            return response('OK', {}, {token});
        } catch (err) {
            return response('ERROR', {message: err.message}, {});
        }
    },
    verToken(token){
        try {
            jwt.verify(token, process.env.SECRET_KEY, { algorithms: ["HS512"] }, (err, decoded) => {
                if(err) throw Error(err.message);
                return response('OK', {}, {decoded});
            });
        } catch (err) {
            return response('ERROR', {message: err.message}, {})
        }
        
    }
}

module.exports = jwtService;