const authRouter = require("./authRoutes");
const mainRouter = require("./mainRoutes");

function routes(app) {
    app.use("/", mainRouter);
    app.use("/api/v1", authRouter);
    app.all("*", (req, res) => {
        return res.status(404).json({
            message: "Not Found",
            data: {},
            errors: {}
        })
    });
}

module.exports = routes;
