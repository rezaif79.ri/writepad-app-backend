const express = require("express");
const mainRouter = express.Router();
const mainController = require('../controllers/mainController');

mainRouter.get("/main", mainController.getMain);

module.exports = mainRouter;
