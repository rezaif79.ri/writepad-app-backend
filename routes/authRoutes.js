const express = require("express");
const validator = require('../middlewares/validator');

const authRouter = express.Router();
const authController = require('../controllers/authController');

authRouter.post("/login", validator('auth'), authController.postLogin);
authRouter.post("/register", validator('auth'), authController.postRegister);

module.exports = authRouter;
