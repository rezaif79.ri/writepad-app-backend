const { check } = require('express-validator');
const jwtService = require('../services/jwtService');
const { response_unauthorized } = require('../utils/response');

const jwtAuth = {
    verify(req, res, next){
        try {
            const { access_token } = req.header;  
            const {status, error, data} = jwtService.verToken(access_token);
            if(status === 'ERROR') throw Error(error.message);
            next()
        } catch (err) {
            return response_unauthorized(res, err.message, err);
        }
    },

}

module.exports = jwtAuth;