const {body} = require('express-validator');

function validator(method){
    switch (method) {
        case 'auth':
            return [
                body('username', "Username required (must string & minimum length 5)")
                    .isString().isLength({min:5}).exists({checkNull:true, checkFalsy:true}),
                body('password', "Password required (must string & minumum length 5)")
                    .isString().isLength({min:5}).exists({checkNull:true, checkFalsy:true})
            ]
            
        default:
            break;
    }
}

module.exports = validator;