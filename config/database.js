/**
 * @file
 * Database configuration.
 *
 * Define database connection information,
 * telling the sequelize to connect to which database.
 */

/**
 * Fetch variable from process environment variable */

// const dotenv = require('dotenv')
// dotenv.config()
require('dotenv').config();
 const {
    // Port that the database listening to
    DATABASE_PORT = "54322",
  
    // Database name
    DATABASE_NAME = "petshop",
  
    // Database username
    DATABASE_USERNAME = "postgres",
  
    // Database password
    DATABASE_PASSWORD = "admin",
  
    // Database host
    DATABASE_HOST = "127.0.0.1",
  } = process.env;

module.exports = {
    development: {
        username: DATABASE_USERNAME,
        password: DATABASE_PASSWORD,
        database: DATABASE_NAME + "_development",
        host: DATABASE_HOST,
        port: DATABASE_PORT,
        dialect: "postgres"
    },
    test: {
        username: DATABASE_USERNAME,
        password: DATABASE_PASSWORD,
        database: DATABASE_NAME + "_test",
        host: DATABASE_HOST,
        port: DATABASE_PORT,
        dialect: "postgres"
    },
    production: {
        username: DATABASE_USERNAME,
        password: DATABASE_PASSWORD,
        database: DATABASE_NAME,
        host: DATABASE_HOST,
        port: DATABASE_PORT,
        dialect: "postgres",
        dialectOptions: {
            ssl: {
              require: true,
              rejectUnauthorized: false
            }
          }
    }
}
