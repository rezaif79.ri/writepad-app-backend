'use strict';
const {
  Model
} = require('sequelize');
const{
  uuid
} = require('uuidv4');
module.exports = (sequelize, DataTypes) => {
  class Note extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      Note.belongsTo(models.User, { foreignKey: 'user_id', as: 'notes'});
    }
  };
  Note.init({
    author_id: DataTypes.UUID,
    title: DataTypes.STRING,
    body: DataTypes.TEXT
  }, {
    sequelize,
    modelName: 'Note',
  });

  return Note;
};