'use strict';
const {
  Model
} = require('sequelize');
const {
  uuid
} = require('uuidv4');
module.exports = (sequelize, DataTypes) => {
  class User extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      User.hasMany(models.Note, {foreignKey: 'author_id', as: 'author'});
    }
  };
  User.init({
    username: DataTypes.UUID,
    password: DataTypes.STRING,
    salt: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'User',
  });

  return User;
};