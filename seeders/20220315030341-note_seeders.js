'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    const arrUser = await queryInterface.sequelize.query('SELECT * FROM "Users";');
    
    const arrNote = arrUser[0].map(u => {
      return {
        author_id: u.id,
        title: 'Hello',
        body: 'Hello World!',
        createdAt: new Date(),
        updatedAt: new Date(),
      };
    });
  
    await queryInterface.bulkInsert('Notes', arrNote);
  
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('Notes', null, {});
  }
}


