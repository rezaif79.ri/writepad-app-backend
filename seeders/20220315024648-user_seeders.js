'use strict';
const bcrypt = require('bcrypt');
module.exports = {
  up: async (queryInterface, Sequelize) => {
   const salt1 = bcrypt.genSaltSync(10);
   const password1 = bcrypt.hashSync('password123', salt1);
   
   const salt2 = bcrypt.genSaltSync(10);
   const password2 = bcrypt.hashSync('password123', salt2);
   await queryInterface.bulkInsert('Users', [
     {
       username: 'Udin123',
       salt: salt1,
       password: password1,
       createdAt: new Date(),
       updatedAt: new Date(),
     },
     {
       username: 'rezaif',
       salt: salt2,
       password: password2,
       createdAt: new Date(),
       updatedAt: new Date(),
     },
   ])
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('Users', null, {});
  }
};
