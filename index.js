const express = require("express");
const expressValidator = require('express-validator');
const dotenv = require("dotenv");
const morgan = require('morgan');
const routes = require("./routes");
const cors = require("cors");

const app = express();

const { PORT = 8000 } = process.env;

app.use(morgan('dev'));
app.use(cors("*"));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
// app.use(expressValidator())
dotenv.config();

app.listen(PORT, () => {
    routes(app);
    console.log(`listening on ${PORT}`);
});
