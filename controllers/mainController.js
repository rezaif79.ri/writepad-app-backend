const { response_success, response_internal_server_error } = require('../utils/response');

const mainController = {
    getMain(req, res){
        try {
            response_success(res, null, "OK");
        } catch (error) {
            response_internal_server_error(res, "Internal Server Error",  error.message);
        }
    }
};

module.exports = mainController;