const { response_created, response_conflict, response_internal_server_error, response_success} = require('../utils/response');
const { validationResult } = require('express-validator');
const auth = require('../services/authService');

const authController = {
    async postRegister(req, res){
        try {
            const errors = validationResult(req);
            if(!errors.isEmpty()) return response_conflict(res, "Validation Error", errors.array());

            const { status, error, data} = await auth.register(req.body);
            if(status === 'ERROR') throw Error(error.message);
            if(status === 'FAIL') return response_conflict(res, error.message, {});
            return response_created(res, data, "User created!");
        } catch (err) {
            return response_internal_server_error(res, err.message, null);
        }
        
    },
    async postLogin(req, res){
        try {
            const errors = validationResult(req);
            if(!errors.isEmpty()) return response_conflict(res, "Validation Error", errors.array());
            
            const { status, error, data} = await auth.login(req.body);
            if(status === 'ERROR') throw Error(error.message);
            if(status === 'FAIL') return response_conflict(res, error.message);
            return response_success(res, data, "Login success!");
        } catch (err) {
            return response_internal_server_error(res, err.message, null);
        }
    }
}

module.exports = authController;